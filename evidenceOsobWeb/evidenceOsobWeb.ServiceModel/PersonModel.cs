using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using evidenceOsobWeb.ServiceModel.Types;


namespace evidenceOsobWeb.ServiceModel
{
  [Route("/get-all-people")]
  public class AllPeople : IReturn<AllPeopleResponse> { }

  public class AllPeopleResponse
  {
    public List<Person> Result { get; set; }
  }


  [Route("/get-person-by-id")]
  [Route("/get-person-by-id/{Id}")]
  public class OnePerson : IReturn<AllPeopleResponse>
  {
    public long Id { get; set; }
  }

  public class OnePersonResponse
  {
    public Person Result { get; set; }
  }

  [Route("/save-person")]//[Route("/new-person", "POST")]
  [Route("/save-person/{Name}/{Surname}/{PersonalID}")]
  [Route("/update-person/{Id}/{Name}/{Surname}/{PersonalID}")]
  public class SavePerson : IReturn<Person>
  {
    public long Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public long PersonalID { get; set; }
  }

  [Route("/delete-person")]
  [Route("/delete-person/{Id}")]//[Route("/delete-person/{Id}", "DELETE")]
  public class DeletePerson : IReturnVoid
  {
    public int Id { get; set; }
  }

}
