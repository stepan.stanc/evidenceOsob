using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack;
using evidenceOsobWeb.ServiceModel;
using ServiceStack.OrmLite;
using evidenceOsobWeb.ServiceModel.Types;

namespace evidenceOsobWeb.ServiceInterface
{
  public class MyServices : Service
  {
    public object Any(AllPeople plp)
    {
      return new AllPeopleResponse { Result = Db.Select<Person>() };
    }

    public object Any(OnePerson plp)
    {
      return new OnePersonResponse { Result = Db.Select<Person>(p => p.Id == plp.Id)[0] };
    }

    public object Any(SavePerson r)//public object Put(SavePerson r)
    {
      var person = new Person();
      if (r.Id.ToString().Length > 0)
      {
        person = new Person { Id = r.Id ,Name = r.Name, Surname = r.Surname, PersonalID = r.PersonalID };
      }
      else
      {
        person = new Person { Name = r.Name, Surname = r.Surname, PersonalID = r.PersonalID };
      }
      
      Db.Save(person);
      return person;
    }

    public void Any(DeletePerson r)
    {
      Db.DeleteById<Person>(r.Id);
    }
  }
}
