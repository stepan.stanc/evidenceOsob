using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Funq;
using ServiceStack;
using ServiceStack.Mvc;
using evidenceOsobWeb.ServiceInterface;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.SqlServer;
using ServiceStack.Data;
using evidenceOsobWeb.ServiceModel.Types;


namespace evidenceOsobWeb
{
  public class AppHost : AppHostBase
  {
    /// <summary>
    /// Base constructor requires a Name and Assembly where web service implementation is located
    /// </summary>
    public AppHost()
        : base("evidenceOsobWeb", typeof(MyServices).Assembly) { }

    /// <summary>
    /// Application specific configuration
    /// This method should initialize any IoC resources utilized by your web service classes.
    /// </summary>
    public override void Configure(Container container)
    {
      SetConfig(new HostConfig
      {
        HandlerFactoryPath = "api",
      });

      container.Register<IDbConnectionFactory>(c => new OrmLiteConnectionFactory(
            AppSettings.GetString("ConnectionString"), SqlServerDialect.Provider));

      using (var db = container.Resolve<IDbConnectionFactory>().Open())
      {

        if (db.CreateTableIfNotExists<Person>())
        {
          //vložení první hodnoty
          db.Insert(new Person { Id = 1, Name = "John", Surname = "Doe", PersonalID = 58050505550879 });
        }

      }
      //Config examples
      //this.Plugins.Add(new PostmanFeature());
      //this.Plugins.Add(new CorsFeature());

      //Set MVC to use the same Funq IOC as ServiceStack
      ControllerBuilder.Current.SetControllerFactory(new FunqControllerFactory(container));
    }
  }
}
