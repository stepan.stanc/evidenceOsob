using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace evideceOsobApp.Types
{
  public class Person
  {
    public long Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public long PersonalID { get; set; }

    public override string ToString()
    {
      return "Name: " + Name + ", Surname: " + Surname + ", PersonalID: " + PersonalID;
    }
  }

  public class AllPeople
  {
    public List<Person> Result { get; set; }
  }

  public class OnePeson
  {
    public Person Result { get; set; }
  }
}
