using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace evidenceOsobWeb.ServiceModel.Types
{
  public class Person
  {
    [AutoIncrement]
    public long Id { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public long PersonalID { get; set; }
  }
}
