using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using ServiceStack.Text;
using evideceOsobApp.Types;

namespace evideceOsobApp.WebClient
{
  public class Rest
  {
    public IServiceClient client = new JsonServiceClient("http://localhost:59733/api/").WithCache();

    //get all people in db
    public List<Person> getPeople()
    {
      string response = client.Get<string>("get-all-people").IndentJson();
      return response.FromJson<AllPeople>().Result;
    }

    //get one person by id
    public Person getPerson(long id)
    {
      string response = client.Get<string>(string.Format("get-person-by-id/{0}", id)).IndentJson();
      return response.FromJson<OnePeson>().Result;
    }

    //insert one person
    public Person insertPerson(Person prs) // w/o id
    {
      string response = client.Get<string>(string.Format("/save-person/{0}/{1}/{2}",prs.Name,prs.Surname,prs.PersonalID)).IndentJson();
      return response.FromJson<Person>();
    }

    //edit everything from one person
    public Person editPerson(Person prs) // w/ id
    {
      string response = client.Get<string>(string.Format("/update-person/{0}/{1}/{2}/{3}",prs.Id, prs.Name, prs.Surname, prs.PersonalID)).IndentJson();
      return response.FromJson<Person>();
    }

    //delete one person
    public void deletePerson(long id)
    {
      string response = client.Get<string>(string.Format("delete-person/{0}", id)).IndentJson();
    }
  }
}
